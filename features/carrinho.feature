# language: pt
Funcionalidade: Carrinho de produtos
  A fim de comprar produtos
  Como um cliente
  Eu preciso colocar produtos do meu interesse no carrinho

  Regras:
  - O imposto é de 20%
  - O frete para um carrinho de compras até R$10 é R$3
  - O frete para um carrinho de compras maior que R$10 é R$2

  Cenário: Comprando um único produto que custe menos que R$10
    Dado que exista um "Sabre de luz do Lorde Sith", que custe R$5
    Quando Eu adicionar o "Sabre de luz do Lorde Sith" ao carrinho
    Então Eu devo ter 1 produto no carrinho
    E o valor total do carrinho deve ser de R$9

  Cenário: Comprando um único produto que custe mais que R$10
    Dado que exista um "Sabre de luz do Lorde Sith", que custe R$15
    Quando Eu adicionar o "Sabre de luz do Lorde Sith" ao carrinho
    Então Eu devo ter 1 produto no carrinho
    E o valor total do carrinho deve ser de R$20

  Cenário: Comprando dois produtos que custem mais que R$10
    Dado que exista um "Sabre de luz do Lorde Sith", que custe R$10
    E que exista um "Sabre de luz Jedi", que custe R$5
    Quando Eu adicionar o "Sabre de luz do Lorde Sith" ao carrinho
    E Eu adicionar o "Sabre de luz Jedi" ao carrinho
    Então Eu devo ter 2 produtos no carrinho
    E o valor total do carrinho deve ser de R$20