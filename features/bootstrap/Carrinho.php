<?php
/**
 * Created by PhpStorm.
 * User: aroldofilho
 * Date: 14/08/18
 * Time: 14:19
 */

class Carrinho implements \Countable
{
    private $prateleira;
    private $produtos;
    private $valoresProdutos = 0.0;


    public function __construct(Prateleira $prateleira){
        $this->prateleira = $prateleira;
    }


    public function adicionaProduto($produto){
        $this->produtos[] = $produto;
        $this->valoresProdutos += $this->prateleira->pegaValorProduto($produto);
    }

    public function pegaValorTotal(){
        return $this->valoresProdutos
            + ($this->valoresProdutos * 0.2)
            + ($this->valoresProdutos > 10 ? 2.0 : 3.0);
    }

    public function count(){
        return count($this->produtos);
    }
}