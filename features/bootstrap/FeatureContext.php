<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Webmozart\Assert\Assert;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{

    private $prateleira;
    private $carrinho;


    public function __construct()
    {
        $this->prateleira = new Prateleira();
        $this->carrinho = new Carrinho($this->prateleira);
    }

    /**
     * @Given que exista um :produto, que custe R$:valor
     */
    public function queExistaUmQueCusteR($produto, $valor)
    {
        $this->prateleira->colocaValorProduto($produto, floatval($valor));
    }

    /**
     * @When Eu adicionar o :produto ao carrinho
     */
    public function euAdicionarOAoCarrinho($produto)
    {
        $this->carrinho->adicionaProduto($produto);
    }

    /**
     * @Then Eu devo ter :quantidade produto(s) no carrinho
     */
    public function euDevoTerProdutoNoCarrinho($quantidade)
    {
        Assert::same(intval($quantidade), $this->carrinho->count(), "Quantidade de produtos diferentes");
    }

    /**
     * @Then o valor total do carrinho deve ser de R$:valor
     */
    public function oValorTotalDoCarrinhoDeveSerDeR($valor)
    {
        Assert::same(floatval($valor), $this->carrinho->pegaValorTotal(), "Valor diferente da quantidade exibida no carrinho");
    }
}
