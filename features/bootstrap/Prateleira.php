<?php
/**
 * Created by PhpStorm.
 * User: aroldofilho
 * Date: 14/08/18
 * Time: 14:19
 */

class Prateleira
{
    private $valores = array();

    public function colocaValorProduto($produto, $valor){
        $this->valores[$produto] = $valor;
    }

    public function pegaValorProduto($produto){
        return $this->valores[$produto];
    }

}